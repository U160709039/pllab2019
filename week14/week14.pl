use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input;

my $regex = '^\s*$'; # match if string $str contains 0 or more white space characters 
$regex = '^[A-Z]+$'; # string $str contains all capital letters (at least one)
$regex = '[A-Z]\d*'; # string $str contains a capital letter followed by 0 or more digits
$regex = '^\d+\.\d+$'; # number $n contains some digits before and after a decimal point
$regex = '^(\d{1,3}\.){3}\d{1,3}$';

until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}

